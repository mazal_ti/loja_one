<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <link rel="stylesheet" href="<?=$base?>/assets/css/style.css"> 
        <title> Loja One</title>
    </head>
    <body>
        <header>
            <div class="header-msg">
                <h3>Devolucao 30 dias </h3>
            </div>
            <div class="logo"> 
                <a href="<?=$base?>/loja_one">LOJA ONE</a> 
            </div>
            <div class="header">
                <div class="menu">
                        <nav>
                            <ul>
                                <li class="action"><a href="">Home</a></li>
                                <li><a href="">BEST SELLERS</a></li>
                                <li><a href="">Novidades</a></li>
                                <li><a href="">Feminino</a></li>
                                <li><a href="">Vestidos</a></li>
                                <li><a href="">Acessorios</a></li>
                                <li><a href="">Masculino</a></li>
                                <li><a href="">Bazar</a></li>
                                <li><a href="">Marcas</a></li>
                                <li><a href="">Sobre</a></li>
                            </ul>
                        </nav>
                    </div>
            </div>
        </header>
  
