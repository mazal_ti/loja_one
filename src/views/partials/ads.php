<style>
    .ads-principal{
        display: flex;
        margin-top: 20px;
    }
    .ads-image{
        width:400px;
        display: flex;
        flex-direction: column;
        align-items: flex-end;
        padding-right: 5px;
    }
    .ads-img img{
        width: 100px;
        height: 130px;
        margin: 0px 10px;
        margin-bottom: 10px;
        border: 2px solid #ccc;
        padding: 6px;
    }

    .ads-image-principal{
        width:638px;
        height:817px;
    }

    .ads-select-caminho{
        font-size: 12px;
        color: #bbb;
        padding: 0 0 6px 0;
    }

    .ads-select-titulo{
        width: 28%;
        font-family: ui-serif
    }

    .ads-select-codigo{
        padding: 11px 0 5px 0;
        font-size: 10px;
    }

    .ads-select-vendedor{
        font-size: 14px;
    }

    .ads-select-preco{
        width: 25%;
        padding: 18px 0;
    }
    
    .ads-select-cores{
        font-size: 10px;
        padding: 0 0 20px;
    }

    .ads-select-tamanhos{
        font-size: 10px;
    }

    .ads-select-tamanhos-select{
        display: flex;
        flex-flow: wrap;
        max-width: 60%;
        line-height: 3;
    }

    .ads-select-cores-{
        padding-right: 70px;
    }

    .ads-select-medidas-btn{
        padding: 10px 19px 10px 19px;
        border: 1px solid #ccc;
    }

    .ads-select-compras{
        display: flex;
    }
    .ads-select-vazio{
        width: 100px;
        height: 817px;
    
    }

</style>

<section>
    <div class="ads-principal">
        <div class="ads-image">
            <div class="ads-img">
                <img src="<?=$base?>/assets/images/images_produtos/1.jpg"/> 
            </div>
            <div class="ads-img">
                <img src="<?=$base?>/assets/images/images_produtos/2.jpg"/>
            </div>
            <div class="ads-img">
                <img src="<?=$base?>/assets/images/images_produtos/3.jpg"/> 
            </div>
            <div class="ads-img">
                <img src="<?=$base?>/assets/images/images_produtos/4.jpg"/> 
            </div>
        </div>

        <div class="ads-image-principal">
            <div class="ads-img-princ">
                <img src="<?=$base?>/assets/images/images_produtos/4.jpg"/> 
            </div>
        </div>

        <div class="ads-compras">
            <div class="ads-select-compras">
                <div class="ads-select-vazio"></div>
                <div class="ads-select-conteudo">
                    <div class="ads-select-caminho"> Zinzane / Feminino / Vestidos / Midi </div>
                    <div class="ads-select-titulo">
                        <h3>Vestido Lurex Busto Transpasse</h3>
                    </div>
                    <div class="ads-select-codigo">
                            <span> 023054 </span>
                    </div>
                    <div class="ads-select-vendedor">
                        <span>Vendedor: </span> <strong> Zinane </strong>
                    </div>
                    <div class="ads-select-preco">
                        <span> R$ 299,99 ou 6x de R$ 49,99 </span>
                    </div>
                    <div class="ads-select-cores">
                        <div class="">
                            <h2>Cor</h2>
                            <input type="radio" class="ads-select-cores-um" /><span>PRETO</span>
                            <!-- <input type="radio" class="ads-select-cores-dois" /><span>ROSA</span>
                            <input type="radio" class="ads-select-cores-tres" /><span>VERDE</span> -->
                        </div>
                    </div>
                    <div class="ads-select-tamanhos">
                        <h2>Tamanho</h2>
                        <div class="ads-select-tamanhos-select">
                            <div class="ads-select-cores-">
                                <input type="radio"/><span>PP</span>
                            </div>
                            <div class="ads-select-cores-" >
                                <input type="radio"/><span>P</span>
                            </div>
                            <div class="ads-select-cores-" >
                                <input type="radio"/><span>M</span>
                            </div>
                            <div class="ads-select-cores-" >
                                <input type="radio"/><span>G</span>
                            </div>
                            <div class="ads-select-cores-" >
                                <input type="radio"/><span>GG</span>
                            </div>
                            <div class="ads-select-cores-" >
                                <input type="radio"/><span>XG</span>
                            </div>
                            <div class="ads-select-cores-" >
                                <input type="radio"/><span>XGG</span>
                            </div>
                        </div>
                    </div>
                    <div class="ads-select-medidas">
                            <input type="button" name="" class="ads-select-medidas-btn" value="Tabela de Medidas"/>
                    </div>
                    <div class="ads-select-comprar-prod">
                        <input type="button" value="COMPRAR" />
                    </div>
                    <div class="ads-select-add-carrinho">
                        <input type="button" value="Adicionar ao carrinho" />
                    </div>
                    <div class="ads-select-comprar-prod">
                        <div>
                            <span>Informe seu CEP para simulador os prazos de entrega </span>
                        </div>
                        <div>
                            <input type="text" placeholder="00000-00" />
                            <input type="button" value="COMPRAR" />
                        </div>
                        <div>
                            <span>Nao sei o CEP</span>
                        </div>
                    </div>


                    <div>
                        <p>- O Vestido lurex é aquela peça desejo superchique e elegante, perfeita para uma ocasião especial.<br>
                        O toque de lurex na peça traz charme e sofisticação para quem a veste.<br> Uma peça incrível, que você pode apostar em acessórios minimalistas e com saltos para uma produção completa.</p>
                        <p>- Confeccionado em viscose acetinada com fios de lurex.</p>
                        <p>- Shape A, comprimento Midi. </p>
                        <p>- Possui decote transpassado fixo frente e costas, possui pala na cintura e detalhe de alças ajustáveis por amarração. </p>
                        <p>- Modelo veste P.</p>
                    </div>
                    <div> 
                        <span>Composicao </span>
                        <div> 
                            <p> 99% Viscose 1% Poliester</p>
                        </div>
                    </div>

                </div>
                
            </div>

        </div>
    </div>
</section>