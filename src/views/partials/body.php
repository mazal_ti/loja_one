<main class="home__main">

    <section class="home-banner">
        <div class="home-banner__sliders">
            <div class="home-banner__slide">
                <div class="home-banner__slidearea">
                    <h1>Design is about  <br/><span>Comunication</span> </h1>
                    <h2> Call Us: +00 0 124 5699</h2>
                    <a href="#"> Get in touch! </a>
                </div>
            </div>
            <div class="home-banner__slide">
                <div class="home-banner__slidearea">
                    <h1>Design is about  <br/><span>Comunication</span> </h1>
                    <h2> Call Us: +00 0 124 5699</h2>
                    <a href="#"> Get in touch! </a>
                </div>
            </div>
            <div class="home-banner__slide">
            <div class="home-banner__slidearea">
                    <h1>Design is about  <br/><span>Comunication</span> </h1>
                    <h2> Call Us: +00 0 124 5699</h2>
                    <a href="#"> Get in touch! </a>
                </div>
            </div>
        </div>
        <div class="home-banner__sliders-cetas">
            <button class="home-banner__sliders-cetas-prev">< </button>
            <button class="home-banner__sliders-cetas-next"> ></button>
        </div>
        <div class="home-banner__sliders-pointers">
            <div class="pointer"></div>
            <div class="pointer"></div>
            <div class="pointer"></div>
        </div>
    </section>

    <!-- <section class="home-banner-collection__container">
        
        <div class="section-title">
            <h2>QUERIDINHOS</h2>
        </div>

        <div class="home-banner-collection__container">

            <div class="home-banner-collection__container_one">

                <div class="home-banner-collection__wrapper">
                    <a href="https://www.zinzane.com.br/1549?map=productClusterIds" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-mari-comfy-1009.png?v=637668743263600000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-mari-comfy-1009.png?v=637668743263600000"
                            data-was-processed="true" >
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
                <div class="home-banner-collection__wrapper">
                    <a href="/1466?map=productClusterIds&amp;O=OrderByReleaseDateDESC" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-best-1009.png?v=637668743253770000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-best-1009.png?v=637668743253770000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text">
                        </div>
                    </a>

                </div>

                <div class="home-banner-collection__wrapper">
                    <a href="/feminino/macacao?O=OrderByReleaseDateDESC#1" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="home-banner-collection__container">
        
        <div class="section-title">
            <h3>Mais vistos</h3>
        </div>

        <div class="home-banner-collection__container">

            <div class="home-banner-collection__container_one">

                <div class="home-banner-collection__wrapper">
                    <a href="https://www.zinzane.com.br/1549?map=productClusterIds" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-mari-comfy-1009.png?v=637668743263600000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-mari-comfy-1009.png?v=637668743263600000"
                            data-was-processed="true" >
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
                <div class="home-banner-collection__wrapper">
                    <a href="/1466?map=productClusterIds&amp;O=OrderByReleaseDateDESC" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-best-1009.png?v=637668743253770000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-best-1009.png?v=637668743253770000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text">
                        </div>
                    </a>

                </div>

                <div class="home-banner-collection__wrapper">
                    <a href="/feminino/macacao?O=OrderByReleaseDateDESC#1" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
                <div class="home-banner-collection__wrapper">
                    <a href="/feminino/macacao?O=OrderByReleaseDateDESC#1" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="home-banner-collection__container">
        
        <div class="section-title">
            <h3>O que outros clientes estão vendo</h3>
        </div>

        <div class="home-banner-collection__container">

            <div class="home-banner-collection__container_one">

                <div class="home-banner-collection__wrapper">
                    <a href="https://www.zinzane.com.br/1549?map=productClusterIds" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-mari-comfy-1009.png?v=637668743263600000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-mari-comfy-1009.png?v=637668743263600000"
                            data-was-processed="true" >
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
                <div class="home-banner-collection__wrapper">
                    <a href="/1466?map=productClusterIds&amp;O=OrderByReleaseDateDESC" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-best-1009.png?v=637668743253770000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-best-1009.png?v=637668743253770000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text">
                        </div>
                    </a>

                </div>

                <div class="home-banner-collection__wrapper">
                    <a href="/feminino/macacao?O=OrderByReleaseDateDESC#1" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
                <div class="home-banner-collection__wrapper">
                    <a href="/feminino/macacao?O=OrderByReleaseDateDESC#1" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    
    <section class="home-banner-collection__container">
        
        <div class="section-title">
            <h3>Mais vendidos em Feminino</h3>
        </div>

        <div class="home-banner-collection__container">

            <div class="home-banner-collection__container_one">

                <div class="home-banner-collection__wrapper">
                    <a href="https://www.zinzane.com.br/1549?map=productClusterIds" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-mari-comfy-1009.png?v=637668743263600000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-mari-comfy-1009.png?v=637668743263600000"
                            data-was-processed="true" >
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
                <div class="home-banner-collection__wrapper">
                    <a href="/1466?map=productClusterIds&amp;O=OrderByReleaseDateDESC" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-best-1009.png?v=637668743253770000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-best-1009.png?v=637668743253770000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text">
                        </div>
                    </a>

                </div>

                <div class="home-banner-collection__wrapper">
                    <a href="/feminino/macacao?O=OrderByReleaseDateDESC#1" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
                <div class="home-banner-collection__wrapper">
                    <a href="/feminino/macacao?O=OrderByReleaseDateDESC#1" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
            </div>
        </div>
    </section>

     
    <section class="home-banner-collection__container">
        
        <div class="section-title">
            <h3>Best Sellers</h3>
        </div>

        <div class="home-banner-collection__container">

            <div class="home-banner-collection__container_one">

                <div class="home-banner-collection__wrapper">
                    <a href="https://www.zinzane.com.br/1549?map=productClusterIds" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-mari-comfy-1009.png?v=637668743263600000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-mari-comfy-1009.png?v=637668743263600000"
                            data-was-processed="true" >
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
                <div class="home-banner-collection__wrapper">
                    <a href="/1466?map=productClusterIds&amp;O=OrderByReleaseDateDESC" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-best-1009.png?v=637668743253770000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-best-1009.png?v=637668743253770000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text">
                        </div>
                    </a>

                </div>

                <div class="home-banner-collection__wrapper">
                    <a href="/feminino/macacao?O=OrderByReleaseDateDESC#1" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
                <div class="home-banner-collection__wrapper">
                    <a href="/feminino/macacao?O=OrderByReleaseDateDESC#1" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-macacao-1009.png?v=637668743257200000"
                            data-was-processed="true">
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="home-banner-collection__container">
        
        <div class="section-title">
            <h3>QUEM SOMOS</h3>
        </div>

        <div class="home-banner-collection__container">

            <div class="home-banner-collection__container_one">

                <div class="home-banner-collection__wrapper_somo">
                    <a href="https://www.zinzane.com.br/1549?map=productClusterIds" title="Zinzane">
                        <img class="vve-image__single js--lazy is--loaded" alt="Zinzane" title="Zinzane"
                            data-src="https://zinzane.vteximg.com.br/arquivos/sec-desk-mari-comfy-1009.png?v=637668743263600000"
                            src="https://zinzane.vteximg.com.br/arquivos/sec-desk-mari-comfy-1009.png?v=637668743263600000"
                            data-was-processed="true" >
                        <div class="home-banner-collection__text"></div>
                    </a>
                </div>
                <div class="home-banner-collection__wrapper_somo">
                    <div>
                        <div>
                            <h3> What is Lorem Ipsum?    </h3>
                        </div>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                        <br>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        <br/> when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                        <br>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </div>  
                 
                        <div class="home-banner-collection__text">
                        </div>
                    </a>

                </div>
              
            </div>
        </div>
    </section>


    <section class="home-banner-collection__container">
        
        <div class="section-title">
            <h3>QUEM SOMOS</h3>
        </div>

        <div class="home-banner-collection__container">

            <div class="home-banner-collection__container_leed">
                <div class="home-banner-collection__wrapper_l">
                    <div>
                        <div>
                            <h3> What is Lorem Ipsum?    </h3>
                        </div>
                        <div >
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            <br>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            <br/> when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                            <br>It has survived not only five centuries, but also the leap into electronic typesetting, 
                            <br/>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </div>  
                 
                        <div class="home-banner-collection__text">
                        </div>
                    </a>

                </div>
              
            </div>
        </div>
    </section> -->
</main>
</body>

</html>